import dbm
import random
import shelve
import string
import webApp
from os import remove
from urllib.parse import parse_qs



def obtainUrl(url):
    if url.startswith('https://'):
        return url
    elif url.startswith('http://'):
        return url
    else:
        url = 'https://' + url
        return url

#generamos una url aleatoria que esté acortada
def shortUrl():
    size = 6
    #generamos una lista con letras y números aleatorios
    destino = random.choices(string.ascii_letters + string.digits, k=size)
    #convertimos la lista en un string
    destino = "".join(destino)
    #añadimos el localhost y la barra
    destino = "http://localhost:2345/" + destino
    return "/" + destino.split("/")[-1], destino

#guardamos en memoria las urls acortadas y las urls originales
def saveUrls(dato, urlcorta, url):
    try:
        #abrimos la base de datos
        data1 = shelve.open("/tmp/values")
        data2 = shelve.open("/tmp/URLs")
    except dbm.error:
        #si no se puede abrir la base de datos, se vacía y se vuelve a crear
        print("Error al abrir fichero, se vacía y se vuelve a crear")
        remove("/tmp/values")
        remove("/tmp/URLs")
        data1 = shelve.open("/tmp/values")
        data2 = shelve.open("/tmp/URLs")
    """ Guardamos en la base de datos las urls 
    acortadas y las urls originales con su correspondiente clave """
    data1[dato] = url
    data2[url] = urlcorta
    #cerramos la base de datos
    data1.close()
    data2.close()

def initValue(values):
    try:
        data1 = shelve.open("/tmp/values")
    except dbm.error:
        print("Error en el fichero, se vacía y se vuelve a crear")
        remove("/tmp/values")
        data1 = shelve.open("/tmp/values")
    for key in data1.keys():
        values[key] = data1[key]
    data1.close()

def initURLs(URLs):
    try:
        data2 = shelve.open("/tmp/URLs")
    except dbm.error:
        print("Error en el fichero, se vacía y se vuelve a crear")
        remove("/tmp/URLs")
        data2 = shelve.open("/tmp/URLs")
    for key in data2.keys():
        URLs[key] = data2[key]
    data2.close()



class RandomShort(webApp.webApp):
    values = {}
    initValue(values)
    URLs = {}
    initURLs(values)

    def parse(self,request):
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = request.split('\r\n\r\n', 2)[1]
        return metodo, recurso,cuerpo
    def process(self,dataRequest):

        (metodo,recurso,cuerpo) = dataRequest

        formulario: str = '<form action="/" method="post">' \
                          + '<ul>' \
                          + '<li>' \
                          + '<label for="name">URL que hay que acortar: </label>' \
                          + '<input type="text" id="name" name="URL" />' \
                          + '</li>' \
                          + '<li class="button">' \
                          + '<button type="submit"> Recortar la URL </button>' \
                          + '</body></html>'

        redireccion: str = '<html><head><meta http-equiv="refresh" content="5; URL=' + str(self.values.get(recurso)) \
                           + '"/></head><body><h1>Redireccionando: ' + str(self.values.get(recurso)) + "<br>" \
                           + "Wait... </h1>" \
                           + "</body></html>"

        if metodo == "POST":
            if cuerpo != "URL=":
                parseargs: dict = parse_qs(cuerpo)
                URL: str = parseargs.get('URL')[0]
                URL = obtainUrl(URL)
                if URL in self.URLs.keys():
                    urlcorta = self.URLs.get(URL)
                    valor = urlcorta.split('http://localhost:1234', 2)[1]
                else:
                    if URL in self.URLs.values():
                        URL = self.values.get(URL.split('http://localhost:1234', 2)[1])
                    (valor, urlcorta) = shortUrl()
                saveUrls(valor, urlcorta, URL)
                # añadimos la url acortada y la url original al diccionario de urls
                self.values[valor] = URL.replace('+', ' ')
                self.URLs[URL] = urlcorta.replace('+', ' ')

        httpcode: str = "200 OK"
        htmlbody: str = "<html><body>"

        if cuerpo != "URL=":
            if recurso == "/":
                if len(self.URLs) != 0:
                    lista: str = "Lista de URLs:"
                    for key in self.values.keys():
                        lista: str = lista + "<ul><li>" + self.values.get(key) + " = http://localhost:1234" + \
                                     key + "</li></ul>"
                    htmlbody += lista
                else:
                    htmlbody += "Introducir URL:"
                htmlbody += formulario
            if recurso in self.values.keys() is not None:
                httpcode = "302 FOUND"
                htmlbody = redireccion
        else:
            httpcode = "404 Not Found"
            htmlbody = "<html><body><h1> ERROR: PRUEBA A RECARGAR LA PÁGINA </h1></body></html>"
        return httpcode, htmlbody


if __name__ == "__main__":
    try:
        testWebApp = RandomShort("localhost",1234)
    except KeyboardInterrupt:
        print("Servidor cerrado")